package etat;

import ordinateur.composants.Composant;

public class Eteint implements Etat{
	@Override
	public void Actionner(Composant Compo) {
		Compo.getAllumerEteindre().setEtat(this);
		
	}
	public String toString() {
		return "Je m'Eteint";
	}
}
