/**
 * classe du composant carte mere
 * 
 * @author Jonathan, Tommy
 */
package ordinateur.composants;
import java.util.ArrayList;


import etat.Allume;
import etat.Etat;
import etat.Eteint;
import ordinateur.composants.*;
import ordinateur.composants.comportements.comportementAllumerEteindre.AllumerEteindre;
import ordinateur.composants.comportements.comportementAllumerEteindre.ComportementAllumerEteindreCM;
public class CarteMere extends Composant {

    private String nomCM;
    private Processeur CPU;
    private RAM ram;
    private CarteGraphique GPU;
    private ArrayList<DisqueDur> HDDs;
    private ArrayList<CableSata> cableSatas;
    private int HDDSize;
    /**
     * Methode constructeur parametre de la carte mere
     * 
     * @param nomCM
     * @param CPU
     * @param ram
     * @param GPU
     */
    public CarteMere(String nomCM, Processeur CPU, RAM ram, CarteGraphique GPU){
        this.nomCM = nomCM;
        this.CPU = CPU;
        this.ram = ram;
        this.GPU = GPU;
        this.HDDs = new ArrayList<>();
        this.cableSatas = new ArrayList<>();
        this.etat =null;
        this.allumerEteindre = new ComportementAllumerEteindreCM(this.etat);
    
        
    }
    
   

   /**
    *  Methode permettant d'obtenir le cpu de la carte mere
    * @return CPU
    */
   
    public Processeur getCPU() {
    	return this.CPU;
    }
    /**
     * Methode permettant de parametrer un cpu
     * @param pCPU
     */
    public void setCPU(Processeur pCPU) {
    	this.CPU = pCPU;
    }
    /**
     * methode permettant d'obtenir un GPU
     * @return GPU
     */
    public CarteGraphique getGPU() {
    	return this.GPU;
    }
    /**
     * Methode permettant de configurer le GPU
     * 
     * @param pGPU
     */
    public void setGPU(CarteGraphique pGPU) {
    	this.GPU = pGPU;
    }
    /**
     * methode permettant d'obtenir la ram
     * @return ram
     */
    public RAM getRAM() {
    	return this.ram;
    }
    /**
     * Methode permettant d'obtenir la liste des disques durs
     * @return
     */
    public ArrayList<DisqueDur> getHDDList(){
    	return this.HDDs;
    }
   /**
    * Methode permettant de parametrer la ram
    * 
    * @param pRAM
    */
    public void setRAM(RAM pRAM) {
    	this.ram = pRAM;
    }


	@Override
	public String getFrequence() {
		return null;
	}

	@Override
	public void setFrequence(String frequence) {
		// TODO Auto-generated method stub
		
	}


}
