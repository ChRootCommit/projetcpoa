package ordinateur.composants;

import etat.Etat;
import etat.Eteint;
import ordinateur.composants.comportements.comportementAllumerEteindre.*;
/**
 * Classe de la Carte Graphique
 * @author Jonathan
 *
 */
public class CarteGraphique extends Composant {

    public String nomCG;
/**
 * Constructeur parametre de la carte graphique
 * @param nomCG
 * @param frequenceCG
 */
    public CarteGraphique(String nomCG, String frequenceCG) {
        this.nomCG = nomCG;
        this.frequence = frequenceCG;
        this.etat = null;
        this.allumerEteindre = new ComportementAllumerEteindreGPU(this.etat);
    }


    
    public String getFrequence() {
    	return this.frequence;
    }



	@Override
	public void setFrequence(String frequence) {
		// TODO Auto-generated method stub
		
		this.frequence = frequence;
		
	}
	
	public String toString() {
		return "Carte graphique: " + this.nomCG + " Etat: " + this.frequence; 
	}

	
}
