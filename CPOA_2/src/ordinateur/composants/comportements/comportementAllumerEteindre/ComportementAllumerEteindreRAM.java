package ordinateur.composants.comportements.comportementAllumerEteindre;

import etat.*;
import java.lang.Exception;

public class ComportementAllumerEteindreRAM  implements AllumerEteindre{
	
	private Etat etat;
	
	public ComportementAllumerEteindreRAM(Etat pEtat) {
		this.etat = pEtat;
	}
	

	
	@Override
	public void setEtat(Etat pEtat) {
		// TODO Auto-generated method stub
		
		this.etat = pEtat;
		System.out.println("RAM: " +  this.etat.toString());
	}

	@Override
	public Etat getEtat() throws Exception {
		if(this.etat == null) {
			throw new Exception("Veuillez Avoir un etat non null");
		}
		// TODO Auto-generated method stub
		return this.etat;
	}
	
	
}
