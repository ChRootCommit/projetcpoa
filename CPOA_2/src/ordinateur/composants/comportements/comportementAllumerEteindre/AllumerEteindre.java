package ordinateur.composants.comportements.comportementAllumerEteindre;
import etat.*;

/**
 * Chaque composant affiche un allumage different d'ou l'utilisation du patron strategie
 * Un etat est associe a ce comportement d'allumage. Donc 
 * @author Jonathan, Tommy
 *
 */

public interface AllumerEteindre {
	
	
	public void setEtat(Etat pEtat);
	public Etat getEtat() throws Exception;
}
