package ordinateur.composants.comportements.comportementAjouterComposant;

import ordinateur.composants.CarteMere;
import ordinateur.composants.Composant;
import ordinateur.composants.DELLX4000;
import ordinateur.composants.RAM;

public class ComportementAjouterRAM implements ComportementAjouter{
	
	private DELLX4000 pc;
	private CarteMere cm;
	
	public ComportementAjouterRAM(DELLX4000 pPC) {
		this.pc = pPC;
		this.cm = pc.getCM();
	}
	
	@Override
	public void ajouterComposant(Composant pCompo) throws Exception {
		if(!(pCompo instanceof RAM)) {
			throw new Exception("Veuillez fournir une ram");
		}
		if(pc.getRAM() != null && cm.getRAM() != null) {
			System.out.println("Veuillez Retirer le composant dej� existant");
		}else {
			this.pc.setRAM((RAM) pCompo);
			this.cm.setRAM((RAM) pCompo);
			pc.AllumerPC();
			System.out.println("Ajout d'un processeur");
		}
		
	}

}
