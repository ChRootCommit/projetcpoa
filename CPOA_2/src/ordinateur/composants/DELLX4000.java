package ordinateur.composants;
import java.util.ArrayList
/**
 * Classe singleton de l'ordinateur
 * @author Jonathan, Tommy
 */
;

import etat.*;
public class DELLX4000{

	private Etat state;
    public CarteGraphique cg = new CarteGraphique("GTX 1060", "1708MHz");
    public Alimentation al ;
    public Processeur cpu = new Processeur("i5-4790k", "2.5");
    public RAM ram = new RAM("1600MHz");
    ArrayList<DisqueDur> HDDs;
    public CarteMere cm = new CarteMere("MSI-315552GL44", cpu, ram, cg);
    private static DELLX4000 Instance = null;
    
/**
 * Constructeur prive 
 */
    private DELLX4000(){
        this.HDDs = new ArrayList<>();
        CableSata cable1 = new CableSata();
        CableSata cable2 = new CableSata();
        CableAlim cable3 = new CableAlim();
        DisqueDur SSD = new DisqueDur("120GO", cable1);
        DisqueDur HDD = new DisqueDur("1To", cable2);
        al = new Alimentation("650W", cable3);
        this.HDDs.add(SSD);
        this.HDDs.add(HDD);
        for(DisqueDur e : HDDs) this.cm.getHDDList().add(e);
    }
    
    /**
     * Methode permettant d'instancier de fa�on unique l'ordinateur.
     * 
     * @return DELLX4000
     */
    
    public static DELLX4000 getInstance() {
    	
    	if(Instance == null) {
    		Instance = new DELLX4000();
    	}
    	return Instance;
    	
    }
    
    /**
     * Methode permettant d'allumer le pc
     */
    public void AllumerPC(){
    	Etat Allumeur = new Allume();
        System.out.println("Je m'allume");
        Allumeur.Actionner(cm);
        Allumeur.Actionner(cpu);
        Allumeur.Actionner(ram);
        Allumeur.Actionner(cg);
        
        for(DisqueDur dd : HDDs) Allumeur.Actionner(dd);
        System.out.println("Je me suis bien allumé");
    }
    /**
     * Methode permettant d'eteindre le pc
     */
    public void EteindrePC(){
    	Etat ShutDowner = new Eteint();
    	System.out.println("Je m'allume");
    	ShutDowner.Actionner(cm);
    	ShutDowner.Actionner(cpu);
    	ShutDowner.Actionner(ram);
    	ShutDowner.Actionner(cg);
    	for(DisqueDur dd : HDDs) ShutDowner.Actionner(dd);
        System.out.println("Je m'eteint");
    }
    
    /**
     * Methode permettant d'obtenir le CPU
     * @return CPU
     */
    public Processeur getCPU() {
    	return this.cpu;
    }
    /**
     * Methode permettant d'obtenir la carte mere du pc
     * @return CarteMere
     */
    public CarteMere getCM() {
    	return this.cm;
    }
    /**
     * Methode permettant de configurer le CPU
     * 
     * @param pCPU
     */
    public void setCPU(Processeur pCPU) {
    	this.cpu = pCPU;
    }
    /**
     * Methode permettant d'obtenir le GPU
     * 
     * @return CarteGraphique
     */
    public CarteGraphique getGPU() {
    	return this.cg;
    }
    
    public void setGPU(CarteGraphique pGPU) {
    	this.cg = pGPU;
    }
    /**
     * Methode permettant d'obtenir la ram
     * @return RAM
     */
    public RAM getRAM() {
    	return this.ram;
    }
    
    public void setRAM(RAM pRAM) {
    	this.ram = pRAM;
    }
    
    /**
     * Methode permettant d'obtenir la liste des disque dur
     * @return HDDs
     */
    public ArrayList<DisqueDur> getHDDList(){
    	return this.HDDs;
    }
    /**
     * Methode permettant d'obtenir la liste des etats
     * @param pEtat
     */
    public void setState(Etat pEtat) {
    	state = pEtat;
    }

}
