package ordinateur.composants;

import etat.Etat;
import etat.Eteint;
import ordinateur.composants.comportements.comportementAllumerEteindre.ComportementAllumerEteindreRAM;
/**
 * Classe de la ram
 */
public class RAM extends Composant{

/***
 * Constructeur parametre de la ram
 * @param frequence
 */
    public RAM(String frequence) {
        this.frequence = frequence;
        this.etat = null;
        this.allumerEteindre = new ComportementAllumerEteindreRAM(this.etat);
    }

    public String getFrequence(){

        return this.frequence;
    }

	@Override
	public void setFrequence(String frequence) {
		// TODO Auto-generated method stub
		this.frequence = frequence;
		
	}

	public String toString() {
		return "ram frequence: " + this.frequence;
	}

}
