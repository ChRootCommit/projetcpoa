package ordinateur.composants;
/**
 *Classe de l'alimentation
 * @author Jonathan, Tommy
 */
import etat.Etat;

public class Alimentation extends Composant {

	private Etat etat;
    public String puissance;
    public CableAlim Alim ;
    /**
     * Constructeur parametre de l'alimentation
     * @param puissance
     * @param Alim
     */
    public Alimentation(String puissance, CableAlim Alim) {
        this.puissance = puissance;
        this.Alim = Alim;
    }
    
    /**
     * Methode permettant d'obtenir le cable d'alimentation
     * @return Alim
     */
    public CableAlim getAlim(){
        return this.Alim;
    }



	@Override
	public String getFrequence() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setFrequence(String frequence) {
		// TODO Auto-generated method stub
		System.out.println("Operation non faisable sur l'alimentation");
	}

	

}
