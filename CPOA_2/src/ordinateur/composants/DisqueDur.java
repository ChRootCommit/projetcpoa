package ordinateur.composants;
import ordinateur.composants.comportements.comportementAllumerEteindre.AllumerEteindre;
import ordinateur.composants.comportements.comportementAllumerEteindre.ComportementAllumerEteindreDD;

import etat.*;
/**
 * Classe du disque dur
 * @author Jonathan, Tommy
 *
 */
public class DisqueDur extends Composant{

    private String capacite;
    private CableSata SATA;
/***
 * Constructeur parametre du disque dur
 * @param capacite
 * @param SATA
 */
    public DisqueDur(String capacite, CableSata SATA) {
        this.capacite = capacite;
        this.SATA = SATA;
        this.allumerEteindre = new ComportementAllumerEteindreDD(this.etat);
        
    }
/**
 * Methode permettant d'obtenir les capacite du disque dur
 * @return capacite
 */
    public String getCapacite(){
        return this.capacite;
    }
/**
 * Methode permettant d'obtenir le cable sata du disque dur
 * @return Sata
 */
    public CableSata getSATA(){
        return this.SATA;
    }

   
	@Override
	public String getFrequence() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setFrequence(String frequence) {
		// TODO Auto-generated method stub
		
	}


		
}
   

