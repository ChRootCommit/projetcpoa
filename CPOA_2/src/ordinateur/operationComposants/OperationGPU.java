package ordinateur.operationComposants;

import ordinateur.composants.Composant;
import ordinateur.composants.DELLX4000;
import ordinateur.composants.comportements.comportementAjouterComposant.ComportementAjouterCPU;
import ordinateur.composants.comportements.comportementAjouterComposant.ComportementAjouterGPU;
import ordinateur.composants.comportements.comportementRetirerComposant.ComportementRetirerGPU;

public class OperationGPU extends OperationComposant {

	public OperationGPU(DELLX4000 pPC) {
		this.pc = pPC;
		this.cm = this.pc.getCM(); 
		this.addComposant = new ComportementAjouterGPU(this.pc);
		this.removeComposant = new ComportementRetirerGPU(this.pc);
	}
	@Override
	public void echangerComposant(Composant pComposant) {
		// TODO Auto-generated method stub
		this.removeComposant.retirerComposant();
		try {
			this.addComposant.ajouterComposant(pComposant);
		} catch(Exception e) {
			System.out.println(e);
		}
	}

}
