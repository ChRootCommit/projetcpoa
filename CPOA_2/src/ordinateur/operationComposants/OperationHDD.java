package ordinateur.operationComposants;

import ordinateur.composants.Composant;
import ordinateur.composants.DELLX4000;
import ordinateur.composants.comportements.comportementAjouterComposant.ComportementAjouterHDD;
import ordinateur.composants.comportements.comportementRetirerComposant.ComportementRetirerHDD;

public class OperationHDD extends OperationComposant{
	
	public OperationHDD(DELLX4000 pPC) {
		this.pc = pPC;
		this.cm = this.pc.getCM(); 
		this.addComposant = new ComportementAjouterHDD(this.pc);
		this.removeComposant = new ComportementRetirerHDD(this.pc);
	}
	
	@Override
	public void echangerComposant(Composant pComposant) {
		// TODO Auto-generated method stub
		System.out.println("Operation non faisable pour les disque dur");
	}
	
	public void echanger2Composants(Composant aEnlever, Composant aAjouter) {
		this.removeComposant.retirerComposantChoix(aEnlever);
		try {
			this.addComposant.ajouterComposant(aAjouter);
		}catch(Exception e) {
			System.out.println(e);
		}
	}
}
